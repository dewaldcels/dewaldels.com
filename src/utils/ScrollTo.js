export const ScrollToRef = (parent, ref, offset = 32) => {
    //parent.current.scrollTo(0, ref.current.offsetTop - offset)

    parent.current.scroll({
        behavior: 'smooth',
        top: ref.current.offsetTop - offset
    })
}