const cardContentStyle = {
    padding: '2rem'
}
const CardContent = ({ children }) => (
    <main style={ cardContentStyle }>{ children }</main>
)
export  default CardContent