import styles from './Card.module.css'

const Card = ({ children }) => (
    <article className={ styles.Card }>
        { children }
    </article>
)

export default Card
